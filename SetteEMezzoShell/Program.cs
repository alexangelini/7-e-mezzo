﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LibreriaSetteEMezzo;

namespace SetteEMezzoShell
{
    class Program
    {
        static void Main(string[] args)
        {
            Gestore gestore;
            Umano umano;
            Banco banco;
            Mazzo mazzo;
            double punteggioUmano;
            double punteggioBanco;
            bool termina = false;
            bool ok = false;

            Console.WriteLine("****************************************************");
            Console.WriteLine("|| \tBenvenuto nel gioco SETTE e MEZZO         ||");
            Console.WriteLine("||...premi un tasto per iniziare a giocare...     ||");
            Console.WriteLine("****************************************************\n");
            Console.ReadKey();

            /* ciclo finché umano vuole giocare */
            while (!termina)
            {
                gestore = new Gestore();
                umano = new Umano();
                banco = new Banco();
                mazzo = new Mazzo();
                ok = false;

                /* esegui umano */
                gestore.ControllaUmanoShell(mazzo, umano);

				/* se umano non ha sballato, esegue banco */
                if (!umano.sballato)
                {
                    punteggioUmano = umano.CalcolaPunteggio();

					gestore.ControllaBanco(mazzo, banco);
                    punteggioBanco = banco.CalcolaPunteggio();

                    /* se umano vince */
                    if (punteggioUmano > punteggioBanco && !umano.sballato || banco.sballato)
                    {
                        Console.WriteLine("\nHai vinto!");
                        Console.WriteLine("Il tuo punteggio è: " + punteggioUmano + ".");
                        Console.WriteLine("Il banco ha sballato.");
                    }
					else if (punteggioBanco >= punteggioUmano && !banco.sballato) /* se banco vince */
                    {
                        Console.WriteLine("\nHai perso! Vince il banco.");
                        Console.WriteLine("Il tuo punteggio è: " + punteggioUmano + ".");
                        Console.WriteLine("Il punteggio del banco è: " + punteggioBanco + ".");
                    }
                    
                }

                /* ciclo per iniziare o terminare partita */
                while (!ok)
                {
                    Console.Write("\nVuoi cominciare un'altra partita?(si/no): ");
                    string scelta = Console.ReadLine();

                    if (scelta.Equals("si"))
                    {
                        ok = true;
                    }
                    else if (scelta.Equals("no"))
                    {
                        termina = true;
                        ok = true;
                    }
                    else
                    {
                        ok = false;
                    }
                }
            }
        }

        
    }
}
