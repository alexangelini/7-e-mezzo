﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibreriaSetteEMezzo;
using System.Threading;

namespace SetteEMezzoGUI
{
    public partial class Form1 : Form
    {

        #region DICHIARAZIONE VARIABILI GLOBALI

        public Gestore gestore;
        public Umano umano;
        public Banco banco;
        public Mazzo mazzo;
        public double punteggioUmano;
        public double punteggioBanco;
        public Carta cartaEstratta;
        public bool termina = false;
        public List<PictureBox> immagini; /* lista contenente le picturebox con le immagini delle carte */
        public Form2 form2; /* form per la scelta del valore del jolly */
        public int pos1x = 15; /* posizione della prima carta di umano */
        public int pos2x = 15; /* posizione della prima carta di banco */

        public event EventHandler<AggiornaPunteggioEventArgs> AggiornaPunteggioUmano; /* evento per aggiornare il punteggio umano in tempo reale */
        public event EventHandler<AggiornaPunteggioEventArgs> AggiornaPunteggioBanco; /* evento per aggiornare il punteggio banco in tempo reale */
        
        
        #endregion


        #region COSTRUTTORE

        public Form1()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            gestore = new Gestore();
            umano = new Umano();
            banco = new Banco();
            mazzo = new Mazzo();
            punteggioUmano = 0;
            punteggioBanco = 0;
            textBoxPunteggioUmano.Text = "0";
            textBoxPunteggioBanco.Text = "0";
            immagini = new List<PictureBox>();
            buttonCarta.Enabled = true;
            buttonStop.Enabled = false;
            form2 = new Form2();
            AggiornaPunteggioUmano += new EventHandler<AggiornaPunteggioEventArgs>(this_AggiornaPunteggioUmano);
            AggiornaPunteggioBanco += new EventHandler<AggiornaPunteggioEventArgs>(this_AggiornaPunteggioBanco);
        }

        #endregion


        #region EVENTI

        /* metodo per iniziare una nuova partita */
        private void Reset()
        {
            gestore = new Gestore();
            umano = new Umano();
            banco = new Banco();
            mazzo = new Mazzo();
            punteggioUmano = 0;
            punteggioBanco = 0;
            /* rimuove tutte le picture box */
            foreach (PictureBox img in immagini)
                img.Dispose();
            immagini.Clear();
            form2 = new Form2();
            textBoxPunteggioUmano.Text = ("0");
            textBoxPunteggioBanco.Text = ("0");
			AggiornaPunteggioUmano += new EventHandler<AggiornaPunteggioEventArgs>(this_AggiornaPunteggioUmano);
			AggiornaPunteggioBanco += new EventHandler<AggiornaPunteggioEventArgs>(this_AggiornaPunteggioBanco);
            buttonCarta.Enabled = true;
            buttonStop.Enabled = false;
            buttonNuovaPartita.Enabled = true;
            termina = false;
            pos1x = 15;
            pos2x = 15;
        }

        /* click pesca carta */
        private void buttonCarta_Click(object sender, EventArgs e)
        {
            AggiornaPunteggioEventArgs aggiornaUmano = new AggiornaPunteggioEventArgs();

            cartaEstratta = mazzo.EstraiCarta();
            /* caso jolly, scelta del valore */
            if (cartaEstratta.semeCarta.Equals("denari") && cartaEstratta.nomeCarta.Equals("re"))
            {
                form2.StartPosition = FormStartPosition.CenterParent;
                form2.ShowDialog();
                double val = form2.GetValore();
                cartaEstratta.valoreCarta = val;
            }

            umano.PrendiCarta(cartaEstratta);
            punteggioUmano = umano.CalcolaPunteggio();

            string stringaCarta = cartaEstratta.nomeCarta + "_" + cartaEstratta.semeCarta;

            /* creazione picturebox e immagine carta */
            PictureBox immagine = new PictureBox();
            Object oggettoImg = Properties.Resources.ResourceManager.GetObject(stringaCarta);

            immagine.Image = (Image)oggettoImg;
            immagine.Location = new System.Drawing.Point(pos1x, 40);
            immagine.Name = stringaCarta;
            immagine.Size = new System.Drawing.Size(75, 150);
            immagine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            immagine.BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(immagine);
            
            immagine.BringToFront();
            pos1x += 40;
            immagini.Add(immagine);
                        
            aggiornaUmano.punteggio = punteggioUmano;
            OnAggiornaPunteggioUmano(aggiornaUmano);

            /* casi punteggio */
            if (punteggioUmano > 7.5)
            {
                umano.sballato = true;
            }
            
            /* se umano sballa, ha perso */
            if (umano.sballato)
            {
                DialogResult perso = MessageBox.Show("Hai sballato!\nVuoi fare un'altra partita?", "Hai perso!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (perso == DialogResult.No)
                    this.Close();
                else
                    Reset();
            }

            if (umano.carteInMano.Count() >= 1)
                buttonStop.Enabled = true;
        }

        /* click stop */
        private void buttonStop_Click(object sender, EventArgs e)
        {
            buttonCarta.Enabled = false;
			buttonStop.Enabled = false;
            buttonNuovaPartita.Enabled = false;
            EseguiBanco();
        }

        /* metodo per eseguire il banco */
        private void EseguiBanco()
        {
            bool gioca = true;
            AggiornaPunteggioEventArgs aggiornaBanco = new AggiornaPunteggioEventArgs();
            
            /* gioca finché banco non ha il punteggio > di umano e non sballa */
            while (gioca && !banco.sballato)
            {
                punteggioBanco = banco.CalcolaPunteggio();
                
                if (punteggioBanco > 7.5)
                {
                    banco.sballato = true;
                    gioca = false;
                }
				else if (punteggioBanco >= punteggioUmano && punteggioBanco <= 7.5)
                {
                    gioca = false;
                }
                else if (punteggioUmano > punteggioBanco)
                {
                    cartaEstratta = mazzo.EstraiCarta();
                    /* caso jolly, assegna un possibile valore */
                    if (cartaEstratta.nomeCarta.Equals("re") && cartaEstratta.semeCarta.Equals("denari"))
                    {
                        gestore.ControllaCartaBancoGUI(banco, cartaEstratta);
                    }

                    banco.PrendiCarta(cartaEstratta);

                    string stringaCarta = cartaEstratta.nomeCarta + "_" + cartaEstratta.semeCarta;

                    /* creazione picturebox e immagine carta */
                    PictureBox immagine = new PictureBox();
                    Object oggettoImg = Properties.Resources.ResourceManager.GetObject(stringaCarta);

                    immagine.Image = (Image)oggettoImg;
                    immagine.Location = new System.Drawing.Point(pos2x, 245);
                    immagine.Name = stringaCarta;
                    immagine.Size = new System.Drawing.Size(75, 150);
                    immagine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
                    immagine.BorderStyle = BorderStyle.FixedSingle;
                    Controls.Add(immagine);

                    immagine.BringToFront();
                    pos2x += 40;
                    immagini.Add(immagine);

                    aggiornaBanco.punteggio = banco.CalcolaPunteggio();
                    OnAggiornaPunteggioBanco(aggiornaBanco);

                    /* visualizza nuova carta ogni secondo */
                    Application.DoEvents();
                    if (punteggioUmano >= aggiornaBanco.punteggio)
                        System.Threading.Thread.Sleep(1000);
                    else
                        System.Threading.Thread.Sleep(500);
                }

                
            }

            /* casi punteggio */
			if (punteggioBanco >= punteggioUmano && !banco.sballato)
            {
                DialogResult perso = MessageBox.Show("Il banco ha vinto!\nVuoi fare un'altra partita?", "Hai vinto!", MessageBoxButtons.YesNo);
                if (perso == DialogResult.No)
                    this.Close();
                else
                    Reset();
            }
            else if (punteggioUmano > punteggioBanco || banco.sballato)
            {
                DialogResult perso = MessageBox.Show("Il banco ha sballato.\nHai vinto!\nVuoi fare un'altra partita?", "Hai vinto!", MessageBoxButtons.YesNo);
                if (perso == DialogResult.No)
                    this.Close();
                else
                    Reset();
            }
            
        }

        /* click nuova partita */
        private void buttonNuovaPartita_Click(object sender, EventArgs e)
        {
            Reset();
        }

        /* metodi per l'aggiornamento dei punteggi */
        protected virtual void OnAggiornaPunteggioUmano(AggiornaPunteggioEventArgs e)
        {
            AggiornaPunteggioUmano(this, e);
        }

        protected virtual void OnAggiornaPunteggioBanco(AggiornaPunteggioEventArgs e)
        {
            AggiornaPunteggioBanco(this, e);
        }

        void this_AggiornaPunteggioUmano(object sender, AggiornaPunteggioEventArgs e)
        {
            textBoxPunteggioUmano.Text = e.punteggio.ToString();
        }

        void this_AggiornaPunteggioBanco(object sender, AggiornaPunteggioEventArgs e)
        {
            textBoxPunteggioBanco.Text = e.punteggio.ToString();
        }

        #endregion        
    }
}
