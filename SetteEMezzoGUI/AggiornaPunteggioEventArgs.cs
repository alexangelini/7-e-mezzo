﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SetteEMezzoGUI
{
    public class AggiornaPunteggioEventArgs : EventArgs
    {
        public double punteggio { get; set; }
    }
}
