﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SetteEMezzoGUI
{
    public partial class Form2 : Form
    {

        public double valore;

        public Form2()
        {
            InitializeComponent();
            valore = 0;
        }

        private void buttonMezzo_Click(object sender, EventArgs e)
        {
            valore = 0.5;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            valore = 1;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            valore = 2;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            valore = 3;
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            valore = 4;
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            valore = 5;
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            valore = 6;
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            valore = 7;
            this.Close();
        }

        public double GetValore()
        {
            return valore;
        }
            
    }
}
