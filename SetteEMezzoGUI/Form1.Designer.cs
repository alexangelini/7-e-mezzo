﻿namespace SetteEMezzoGUI
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_carte_banco = new System.Windows.Forms.Panel();
            this.buttonCarta = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelPunteggioGiocatore = new System.Windows.Forms.Label();
            this.labelPunteggioBanco = new System.Windows.Forms.Label();
            this.buttonNuovaPartita = new System.Windows.Forms.Button();
            this.labelGiocatore = new System.Windows.Forms.Label();
            this.labelBanco = new System.Windows.Forms.Label();
            this.panel_carte_giocatore = new System.Windows.Forms.Panel();
            this.textBoxPunteggioUmano = new System.Windows.Forms.TextBox();
            this.textBoxPunteggioBanco = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // panel_carte_banco
            // 
            this.panel_carte_banco.BackColor = System.Drawing.Color.DarkGreen;
            this.panel_carte_banco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_carte_banco.Location = new System.Drawing.Point(12, 237);
            this.panel_carte_banco.Name = "panel_carte_banco";
            this.panel_carte_banco.Size = new System.Drawing.Size(600, 170);
            this.panel_carte_banco.TabIndex = 1;
            // 
            // buttonCarta
            // 
            this.buttonCarta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarta.Location = new System.Drawing.Point(68, 424);
            this.buttonCarta.Name = "buttonCarta";
            this.buttonCarta.Size = new System.Drawing.Size(160, 45);
            this.buttonCarta.TabIndex = 2;
            this.buttonCarta.Text = "PESCA CARTA";
            this.buttonCarta.UseVisualStyleBackColor = true;
            this.buttonCarta.Click += new System.EventHandler(this.buttonCarta_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStop.Location = new System.Drawing.Point(247, 424);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(160, 45);
            this.buttonStop.TabIndex = 4;
            this.buttonStop.Text = "STOP";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelPunteggioGiocatore
            // 
            this.labelPunteggioGiocatore.AutoSize = true;
            this.labelPunteggioGiocatore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPunteggioGiocatore.Location = new System.Drawing.Point(646, 47);
            this.labelPunteggioGiocatore.Name = "labelPunteggioGiocatore";
            this.labelPunteggioGiocatore.Size = new System.Drawing.Size(105, 17);
            this.labelPunteggioGiocatore.TabIndex = 6;
            this.labelPunteggioGiocatore.Text = "Il tuo punteggio";
            // 
            // labelPunteggioBanco
            // 
            this.labelPunteggioBanco.AutoSize = true;
            this.labelPunteggioBanco.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPunteggioBanco.Location = new System.Drawing.Point(625, 255);
            this.labelPunteggioBanco.Name = "labelPunteggioBanco";
            this.labelPunteggioBanco.Size = new System.Drawing.Size(147, 17);
            this.labelPunteggioBanco.TabIndex = 7;
            this.labelPunteggioBanco.Text = "Il punteggio del banco";
            // 
            // buttonNuovaPartita
            // 
            this.buttonNuovaPartita.Location = new System.Drawing.Point(612, 424);
            this.buttonNuovaPartita.Name = "buttonNuovaPartita";
            this.buttonNuovaPartita.Size = new System.Drawing.Size(160, 45);
            this.buttonNuovaPartita.TabIndex = 8;
            this.buttonNuovaPartita.Text = "Nuova partita";
            this.buttonNuovaPartita.UseVisualStyleBackColor = true;
            this.buttonNuovaPartita.Click += new System.EventHandler(this.buttonNuovaPartita_Click);
            // 
            // labelGiocatore
            // 
            this.labelGiocatore.AutoSize = true;
            this.labelGiocatore.Location = new System.Drawing.Point(13, 11);
            this.labelGiocatore.Name = "labelGiocatore";
            this.labelGiocatore.Size = new System.Drawing.Size(64, 13);
            this.labelGiocatore.TabIndex = 9;
            this.labelGiocatore.Text = "Le tue carte";
            // 
            // labelBanco
            // 
            this.labelBanco.AutoSize = true;
            this.labelBanco.Location = new System.Drawing.Point(16, 218);
            this.labelBanco.Name = "labelBanco";
            this.labelBanco.Size = new System.Drawing.Size(96, 13);
            this.labelBanco.TabIndex = 10;
            this.labelBanco.Text = "Le carte del banco";
            // 
            // panel_carte_giocatore
            // 
            this.panel_carte_giocatore.BackColor = System.Drawing.Color.DarkGreen;
            this.panel_carte_giocatore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_carte_giocatore.Location = new System.Drawing.Point(12, 30);
            this.panel_carte_giocatore.Name = "panel_carte_giocatore";
            this.panel_carte_giocatore.Size = new System.Drawing.Size(600, 170);
            this.panel_carte_giocatore.TabIndex = 0;
            // 
            // textBoxPunteggioUmano
            // 
            this.textBoxPunteggioUmano.BackColor = System.Drawing.Color.Gold;
            this.textBoxPunteggioUmano.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPunteggioUmano.Enabled = false;
            this.textBoxPunteggioUmano.Font = new System.Drawing.Font("Verdana", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPunteggioUmano.ForeColor = System.Drawing.Color.Black;
            this.textBoxPunteggioUmano.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxPunteggioUmano.Location = new System.Drawing.Point(628, 74);
            this.textBoxPunteggioUmano.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxPunteggioUmano.Name = "textBoxPunteggioUmano";
            this.textBoxPunteggioUmano.ReadOnly = true;
            this.textBoxPunteggioUmano.Size = new System.Drawing.Size(140, 72);
            this.textBoxPunteggioUmano.TabIndex = 11;
            this.textBoxPunteggioUmano.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxPunteggioBanco
            // 
            this.textBoxPunteggioBanco.BackColor = System.Drawing.Color.Gold;
            this.textBoxPunteggioBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPunteggioBanco.Enabled = false;
            this.textBoxPunteggioBanco.Font = new System.Drawing.Font("Verdana", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPunteggioBanco.ForeColor = System.Drawing.Color.Black;
            this.textBoxPunteggioBanco.Location = new System.Drawing.Point(628, 283);
            this.textBoxPunteggioBanco.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxPunteggioBanco.Name = "textBoxPunteggioBanco";
            this.textBoxPunteggioBanco.ReadOnly = true;
            this.textBoxPunteggioBanco.Size = new System.Drawing.Size(140, 72);
            this.textBoxPunteggioBanco.TabIndex = 12;
            this.textBoxPunteggioBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 482);
            this.Controls.Add(this.textBoxPunteggioBanco);
            this.Controls.Add(this.textBoxPunteggioUmano);
            this.Controls.Add(this.labelBanco);
            this.Controls.Add(this.labelGiocatore);
            this.Controls.Add(this.buttonNuovaPartita);
            this.Controls.Add(this.labelPunteggioBanco);
            this.Controls.Add(this.labelPunteggioGiocatore);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonCarta);
            this.Controls.Add(this.panel_carte_giocatore);
            this.Controls.Add(this.panel_carte_banco);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Sette e mezzo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_carte_banco;
        private System.Windows.Forms.Button buttonCarta;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelPunteggioGiocatore;
        private System.Windows.Forms.Label labelPunteggioBanco;
        private System.Windows.Forms.Button buttonNuovaPartita;
        private System.Windows.Forms.Label labelGiocatore;
        private System.Windows.Forms.Label labelBanco;
        private System.Windows.Forms.Panel panel_carte_giocatore;
        private System.Windows.Forms.TextBox textBoxPunteggioUmano;
        private System.Windows.Forms.TextBox textBoxPunteggioBanco;

    }
}

