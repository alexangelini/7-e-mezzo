﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaSetteEMezzo
{
    public class Carta
    {
		private double valore;
        private readonly string seme;
        private readonly string nome;

        /* costruttore */
        public Carta()
        {
        }

        /* costruttore */
		public Carta(double valore, string seme, string nome)
        {
            this.valore = valore;
            this.seme = seme;
            this.nome = nome;
        }

        /* metodo che ritorna il valore della carta. es: 0.5 */
		public double valoreCarta
        {
            get { return valore; }
            set { valore = value; }
        }

        /* metodo che ritorna il seme della carta. es: denari */
		public string semeCarta
        {
            get { return seme; }
        }

        /* metodo che ritorna il nome della carta. es: quattro */
		public string nomeCarta
        {
            get { return nome; }
        }

		public override string ToString()
		{
			return string.Format ("Carta: {0} di {1}, valore = {2}", nomeCarta, semeCarta, valoreCarta);
		}

    }
}
