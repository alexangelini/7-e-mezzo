﻿using System;
using System.Threading;

namespace LibreriaSetteEMezzo
{
	public class Banco : Giocatore
	{
        /* costruttore */
        public Banco ()
		{
		}

        public override void StampaPunteggio(double punteggio)
        {
            Console.WriteLine("Il punteggio del banco è: " + punteggio + ".");
        }

	}
}

