﻿using System;

namespace LibreriaSetteEMezzo
{
	public class Umano : Giocatore
	{
        /* costruttore */
		public Umano ()
		{
		}

        public override void StampaPunteggio(double punteggio)
        {
            Console.WriteLine("Il tuo punteggio è: " + punteggio + ".");
        }
	}
}

