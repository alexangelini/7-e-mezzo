﻿using System;

namespace LibreriaSetteEMezzo
{
	public class Gestore
	{
        Carta tmpCarta;
        double[] val = new double[8] { 0.5, 1, 2, 3, 4, 5, 6, 7 }; /* possibili valori delle carte */
        double punteggioUmano = 0;
        double punteggiBanco = 0;

        /* costruttore */
		public Gestore ()
		{
		}

        /* metodo per dare una carta al giocatore umano */
		public void DaiCartaUmano(Mazzo m, Umano u)
		{
            tmpCarta = m.EstraiCarta(); /* estrae una carta casuale dal mazzo */
            ControllaCartaShell(tmpCarta); /* controlla la carta se è il jolly */
            u.PrendiCarta(tmpCarta); /* inserisce la carta nella mano del giocatore umano */
            punteggioUmano = u.CalcolaPunteggio(); /* calcola il punteggio dell'umano */
            u.StampaPunteggio(punteggioUmano); /* stampa in console il punteggio */
		}

        /* metodo per dare una carta al banco */
		public void DaiCartaBanco(Mazzo m, Banco b)
		{
            tmpCarta = m.EstraiCarta(); /* estrae una carta casuale dal mazzo */
            ControllaCartaBanco(tmpCarta, b); /* controlla la carta se è il jolly */
            b.PrendiCarta(tmpCarta); /* inserisce la carta nella mano del banco */
            punteggiBanco = b.CalcolaPunteggio(); /* calcola il punteggio dell'umano */
            b.StampaPunteggio(punteggiBanco); /* stampa in console il punteggio */
		}
        
        /* metodo che effettua il controllo della carta, e nel caso sia il jolly, chiede in input il valore */
        public void ControllaCartaShell(Carta carta)
        {
            if (carta.nomeCarta.Equals("re") && carta.semeCarta.Equals("denari")) /* se è il jolly */
            {
                double tmpDouble = 0;
                bool inputOk = false;
                string tmpStr;

                Console.WriteLine("La carta estratta è il jolly: " + carta.nomeCarta.ToUpper() + " di " + carta.semeCarta.ToUpper() + ".");
                Console.Write("Inserire il valore da attribuire al jolly(0.5, 1, 2, 3, 4, 5, 6, 7): ");

                /* ciclo finché il valore inserito non è corretto */
                while (!inputOk)
                {
                    tmpStr = Console.ReadLine();
                    if (tmpStr.Equals("0.5"))
                    {
                        tmpDouble = double.Parse(tmpStr, System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (tmpStr.Equals("0")
                             || tmpStr.Equals("1")
                             || tmpStr.Equals("2")
                             || tmpStr.Equals("3")
                             || tmpStr.Equals("4")
                             || tmpStr.Equals("5")
                             || tmpStr.Equals("6")
                             || tmpStr.Equals("7"))
                    {
                        double.TryParse(tmpStr, out tmpDouble);
                    }
                    else
                    {
                        Console.WriteLine("Valore inserito non corretto.");
                        Console.Write("Inserire il valore da attribuire al jolly(0.5, 1, 2, 3, 4, 5, 6, 7): ");
                    }

                    foreach (double d in val)
                    {
                        if (tmpDouble == d)
                        {
                            carta.valoreCarta = tmpDouble;
                            inputOk = true;
                        }
                    }
                }

            }
            else /* se non è il jolly, stampa la carta estratta */
            {
                Console.WriteLine("\nLa carta estratta è " + carta.nomeCarta.ToUpper() + " di " + carta.semeCarta.ToUpper() + ", valore: " + carta.valoreCarta + ".");
            }
        }

		/* metodo che effettua il controllo della carta del banco e assegna il valore più giusto nel caso sia il jolly */
        public void ControllaCartaBanco(Carta carta, Banco b)
        {
            if (carta.nomeCarta.Equals("re") && carta.semeCarta.Equals("denari")) /* se è il jolly */
            {
                double tmpP;
                tmpP = b.CalcolaPunteggio();
                Random rnd = new Random();
                int index;

                Console.WriteLine("La carta estratta dal banco è il jolly: " + carta.nomeCarta.ToUpper() + " di " + carta.semeCarta.ToUpper() + ".");

                /* se il punteggio è 0, assegna al jolly un valore casuale */
                if (tmpP == 0)
                {
                    index = rnd.Next(0, val.Length);
                    carta.valoreCarta = val[index];
                }
                /* se il punteggio è x.5, fa 7.5 */
                else if (tmpP == 0.5
                        || tmpP == 1.5
                        || tmpP == 2.5
                        || tmpP == 3.5
                        || tmpP == 4.5
                        || tmpP == 5.5
                        || tmpP == 6.5)
                {
                    carta.valoreCarta = 7.5 - tmpP;
                }
                else if (tmpP == 7)
                {
                    carta.valoreCarta = 0.5;
                }
                /* calcola il valore giusto da assegnare senza sballare */
                else
                {
                    bool ok = false;
                    index = rnd.Next(0, val.Length - 1);
                    carta.valoreCarta = val[index];

                    while (!ok)
                    {
                        if (carta.valoreCarta + tmpP <= 7.5)
                        {
                            ok = true;
                        }
                        else
                        {
                            index = rnd.Next(0, val.Length - 1);
                            carta.valoreCarta = val[index];
                        }
                    }
                }

                Console.WriteLine("Il banco ha attribuito al jolly il valore di: " + carta.valoreCarta + ".");
            }
            else /* se non è il jolly, stampa la carta estratta */
            {
                Console.WriteLine("La carta estratta da Banco è " + carta.nomeCarta.ToUpper() + " di " + carta.semeCarta.ToUpper() + ", valore: " + carta.valoreCarta + ".");
            }
        }

        /* metodo che controlla l'esecuzione dell'umano */
        public void ControllaUmanoShell(Mazzo m, Umano u)
        {
            bool gioca = true;
            string scelta;
            double punteggio_giocatore = u.punteggio;

            DaiCartaUmano(m, u);

            /* ciclo finché vuole una carta e finché non sballa */
            while (gioca && !u.sballato)
            {
                punteggio_giocatore = u.punteggio;

                if (punteggio_giocatore > 7.5)
                {
                    u.sballato = true;
                    gioca = false;
                    Console.WriteLine("Hai sballato! Il banco vince.");
                }
                else
                {
                    Console.Write("\nVuoi un'altra carta?(si/no): ");
                    scelta = Console.ReadLine();

                    if (scelta.Equals("si"))
                    {
                        DaiCartaUmano(m, u);
                    }
                    else if (scelta.Equals("no"))
                    {
                        gioca = false;
                    }
                    else
                    {
                        gioca = true;
                    }
                }
            }
        }

        /* metodo per controllare l'esecuzione del banco */
        public void ControllaBanco(Mazzo m, Banco b)
        {
            bool gioca = true;
            double punteggioBanco = b.punteggio;

            Console.WriteLine("\nIl banco pesca una carta...");
            DaiCartaBanco(m, b);

            /*Console.WriteLine();
            Carta a = new Carta(0, "denari", "re");
            ControllaCartaBanco(a, b);
            b.PrendiCarta(a);
            punteggio_giocatore = b.CalcolaPunteggio();
            b.StampaPunteggio(punteggio_giocatore);*/ /* DEBUG estrarre jolly */

            /* ciclo finché non supero il punteggio dell'umano e non sballo */
            while (gioca && !b.sballato)
            {
                punteggioBanco = b.punteggio;

                if (punteggiBanco > 7.5)
                {
                    b.sballato = true;
                    gioca = false;
                }
                else if (punteggioBanco > punteggioUmano && punteggioBanco <= 7.5)
                {
                    gioca = false;
                }
                else if (punteggioUmano > punteggioBanco)
                {
                    Console.WriteLine("\nIl banco pesca una carta...");
                    DaiCartaBanco(m, b);
                }
            }
        }

        /* metodo per controllare la carta del banco nella GUI */
        public void ControllaCartaBancoGUI(Banco banco, Carta cartaEstratta)
        {
            double tmpP;
            tmpP = banco.CalcolaPunteggio();
            Random rnd = new Random();
            int index;

            /* stessa logica di controlla carta banco */
            if (tmpP == 0)
            {
                index = rnd.Next(0, val.Length);
                cartaEstratta.valoreCarta = val[index];
            }
            else if (tmpP == 0.5
                    || tmpP == 1.5
                    || tmpP == 2.5
                    || tmpP == 3.5
                    || tmpP == 4.5
                    || tmpP == 5.5
                    || tmpP == 6.5)
            {
                cartaEstratta.valoreCarta = 7.5 - tmpP;
            }
            else if (tmpP == 7)
            {
                cartaEstratta.valoreCarta = 0.5;
            }
            else
            {
                bool ok = false;
                index = rnd.Next(0, val.Length - 1);
                cartaEstratta.valoreCarta = val[index];

                while (!ok)
                {
                    if (cartaEstratta.valoreCarta + tmpP <= 7.5)
                    {
                        ok = true;
                    }
                    else
                    {
                        index = rnd.Next(0, val.Length - 1);
                        cartaEstratta.valoreCarta = val[index];
                    }
                }
            }
        }
	}
}

