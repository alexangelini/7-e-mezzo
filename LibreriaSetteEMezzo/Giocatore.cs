﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaSetteEMezzo
{
    public abstract class Giocatore
    {
		public List<Carta> carteInMano = new List<Carta>();
		public double punteggio;
		public bool sballato { get; set; }

        /* costruttore */
		public Giocatore()
        {
			sballato = false;
        }

        /* metodo che aggiunge la carta alle carte in mano */
		public void PrendiCarta(Carta cartaEstratta)
		{
			this.carteInMano.Add(cartaEstratta);
		}

		public string StampaCarteInMano()
		{
			StringBuilder stringaMano = new StringBuilder();
			foreach (Carta c in carteInMano)
			{
				stringaMano.Append(" " + c.nomeCarta + " di " + c.semeCarta);
			}
			return stringaMano.ToString();
		}

        /* calcola il punteggio sommando il valore delle carte in mano */
		public double CalcolaPunteggio()
		{
			punteggio = 0;

			for (int i = 0; i < carteInMano.Count (); i++)
			{
                Carta tmpVal = (Carta)carteInMano[i];
				punteggio += tmpVal.valoreCarta;
			}

            return punteggio; 
		}

        /* metodo per stampare in console il punteggio */
        public virtual void StampaPunteggio(double punteggio)
        {
            Console.WriteLine("Il punteggio è: " + punteggio + ".");
        }
    }
}
