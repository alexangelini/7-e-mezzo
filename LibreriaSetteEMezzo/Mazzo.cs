﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LibreriaSetteEMezzo
{
	public class Mazzo
    {
        private const int MAXCARTE = 40;
        private String[] seme = new String[4] { "bastoni", "coppe", "denari", "spade" };
        private String[] nome = new String[10] { "asso", "due", "tre", "quattro", "cinque", "sei", "sette", "fante", "cavallo", "re" };
        private ArrayList mazzo = new ArrayList();
        private Random rnd = new Random();
        

        /* costruttore */
        public Mazzo()
        {
            /* crea il mazzo */
            for (int s = 0; s < seme.Count(); s++)
            {
                for (int n = 0, v = 1; n < nome.Count(); n++)
                {
                    if (n <= 6)
                    {
                        mazzo.Add(new Carta(v, seme[s], nome[n]));
                        v++;
                    }
                    else if(s == 2 && n == 9)/* al re di denari assegna il valore 0 perché è il jolly */
                    {
                        mazzo.Add(new Carta(0, seme[s], nome[n]));
                    }
                    else
                    {
						mazzo.Add(new Carta(0.5, seme[s], nome[n]));
                    }
                }
            }

        }

        /* metodo ricorsivo che estrae una carta dal mazzo */
		public Carta EstraiCarta()
        {
        	Carta cartaEstratta;
            int numeroRnd = rnd.Next(0, MAXCARTE);

            if(numeroRnd < mazzo.Count)
            {
                cartaEstratta = (Carta)mazzo[numeroRnd];
                mazzo.Remove(mazzo[numeroRnd]);
            }
            else
            {
				cartaEstratta = this.EstraiCarta();
            }

            return cartaEstratta;
        }

    }
}
